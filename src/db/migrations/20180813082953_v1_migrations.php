<?php


use Phinx\Migration\AbstractMigration;

class V1Migrations extends AbstractMigration
{

    /** 
     * Ignoramos el uso del método change() ya que si usamos la función
     * up() escribimos las migraciones en ella y en down() escribiremos
     * las acciones si necesitamos realizar un rollback
    */

    /**
     * Método para escribir las migraciones
     */
    public function up () {

        $table = $this->table('artists');
        $table->addColumn('first_name', 'string', [
                'limit' => 250,
                'null' => false,
        ])
            ->addColumn('last_name', 'string', [
                'default' => null,
                'limit' => 250,
                'null' => true,
            ])
            ->addColumn('birth_date', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('birth_place', 'string', [
                'default' => null,
                'limit' => 250,
                'null' => true,
            ])
            ->addColumn('biography', 'text', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('web', 'string', [
                'default' => null,
                'limit' => 250,
                'null' => true,
            ])
            ->addIndex(['first_name','last_name'])
            ->addIndex(['last_name','first_name'])
            ->addIndex(['birth_date'])
            ->addIndex(['birth_place'])
            ->addIndex(['web'])
            ->save();

        $table = $this->table('typebands');
        $table->addColumn('name', 'string', [
                'limit' => 50,
                'null' => false,
            ])
            ->addIndex(['name'])
            ->save();

        $table = $this->table('bands');
        $table->addColumn('typeband_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('typeband_id', 'typebands', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('name', 'string', [
                'limit' => 250,
                'null' => false,
            ])
            ->addColumn('foundation_date', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('web', 'string', [
                'default' => null,
                'limit' => 250,
                'null' => true,
            ])
            ->addIndex(['id','typeband_id'],['unique' => true])
            ->addIndex(['name'])
            ->addIndex(['foundation_date'])
            ->addIndex(['web'])
            ->save();

        $table = $this->table('roles');
        $table->addColumn('name', 'string', [
                'limit' => 50,
                'null' => false,
            ])
            ->addIndex(['name'])
            ->save();

        $table = $this->table('bands_artists');
        $table->addColumn('artist_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('artist_id', 'artists', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('band_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('band_id', 'bands', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('role_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('role_id', 'roles', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('start_date', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('end_date', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(['start_date','end_date'])
            ->addIndex(['end_date','start_date'])
            ->save();

        $table = $this->table('typeitems');
        $table->addColumn('name', 'string', [
                'limit' => 50,
                'null' => false,
            ])
            ->addIndex(['name'])
            ->save();

        $table = $this->table('items');
        $table->addColumn('typeitem_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('typeitem_id', 'typeitems', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('name', 'string', [
                'limit' => 250,
                'null' => false,
            ])
            ->addColumn('band_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('band_id', 'bands', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('url', 'string', [
                'default' => null,
                'limit' => 250,
                'null' => true,
            ])
            ->addIndex(['id','typeitem_id'],['unique' => true])
            ->addIndex(['name'])
            ->addIndex(['url'])
            ->addIndex(['band_id','name'])
            ->save();

        $table = $this->table('albums');
        $table->addColumn('name', 'string', [
                'limit' => 250,
                'null' => false,
            ])
            ->addColumn('publication_date', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('web', 'string', [
                'default' => null,
                'limit' => 250,
                'null' => true,
            ])
            ->addIndex(['name'])
            ->addIndex(['publication_date'])
            ->addIndex(['web'])
            ->save();

        $table = $this->table('items_albums');
        $table->addColumn('album_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('album_id', 'albums', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('item_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('item_id', 'items', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('typeitem_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('typeitem_id', 'typeitems', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('position', 'integer', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addIndex(['album_id','position'])
            ->addIndex(['album_id','item_id'])
            ->addIndex(['item_id','album_id'])
            ->addIndex(['album_id','typeitem_id','position'],['unique' => true])
            ->save();
    }

    /**
     * Método para escribir los cambios a realizar en caso de generar un rollback
     */
    public function down () {
        //Ojo cuando hacemos un rollback, eliminar primero las tablas con claves
        //ajenas y después las tablas maestras.

        $this->dropTable('bands_artists');
        $this->dropTable('items_albums');
        $this->dropTable('artists');
        $this->dropTable('roles');
        $this->dropTable('items');
        $this->dropTable('bands');
        $this->dropTable('typebands');
        $this->dropTable('typeitems');
        $this->dropTable('albums');
    }

}
