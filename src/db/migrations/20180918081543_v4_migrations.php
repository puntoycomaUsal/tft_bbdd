<?php


use Phinx\Migration\AbstractMigration;

class V4Migrations extends AbstractMigration
{
    /** 
     * Ignoramos el uso del método change() ya que si usamos la función
     * up() escribimos las migraciones en ella y en down() escribiremos
     * las acciones si necesitamos realizar un rollback
    */

    /**
     * Método para escribir las migraciones
     */
    public function up () {

        $table = $this->table('tags');
        $table->addColumn('name', 'string', [
                'limit' => 150,
                'null' => false,
            ])
            ->save();

        $table = $this->table('tags_albums');
        $table->addColumn('tag_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('tag_id', 'tags', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('album_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('album_id', 'albums', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addIndex(['tag_id','album_id'])
            ->addIndex(['album_id','tag_id'])
            ->save();

        $table = $this->table('tags_bands');
        $table->addColumn('tag_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('tag_id', 'tags', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('band_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('band_id', 'bands', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addIndex(['tag_id','band_id'])
            ->addIndex(['band_id','tag_id'])
            ->save();
    }

    /**
     * Método para escribir los cambios a realizar en caso de generar un rollback
     */
    public function down () {
        //Ojo cuando hacemos un rollback, eliminar primero las tablas con claves
        //ajenas y después las tablas maestras.

        $this->dropTable('tags');
        $this->dropTable('tags_albums');
        $this->dropTable('tags_bands');
    }
}
