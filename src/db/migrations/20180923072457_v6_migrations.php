<?php


use Phinx\Migration\AbstractMigration;

class V6Migrations extends AbstractMigration
{

    /** 
     * Ignoramos el uso del método change() ya que si usamos la función
     * up() escribimos las migraciones en ella y en down() escribiremos
     * las acciones si necesitamos realizar un rollback
    */

    /**
     * Método para escribir las migraciones
     */
    public function up () {
        $table = $this->table('configurations');
        $table->addColumn('rrss', 'text', [
                'default' => null,
                'null' => true,
            ])
            ->save();

    }

    /**
     * Método para escribir los cambios a realizar en caso de generar un rollback
     */
    public function down () {
        //Ojo cuando hacemos un rollback, eliminar primero las tablas con claves
        //ajenas y después las tablas maestras.
       $table = $this->table('configurations');
       $table->removeColumn('rrss')
              ->save();

    }

}
