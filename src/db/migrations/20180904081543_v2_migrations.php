<?php


use Phinx\Migration\AbstractMigration;

class V2Migrations extends AbstractMigration
{
    /** 
     * Ignoramos el uso del método change() ya que si usamos la función
     * up() escribimos las migraciones en ella y en down() escribiremos
     * las acciones si necesitamos realizar un rollback
    */

    /**
     * Método para escribir las migraciones
     */
    public function up () {

        $table = $this->table('configurations');
        $table->addColumn('pathResources', 'string', [
                'limit' => 250,
                'null' => true,
            ])
            ->addColumn('urlResources', 'string', [
                'limit' => 250,
                'null' => true,
            ])
            ->addColumn('urlApi', 'string', [
                'limit' => 250,
                'null' => true,
            ])
            ->addColumn('endpoints', 'text', [
                'default' => null,
                'null' => true,
            ])
            ->save();


        $table = $this->table('items_artists');
        $table->addColumn('artist_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('artist_id', 'artists', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('item_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('item_id', 'items', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('typeitem_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('typeitem_id', 'typeitems', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('position', 'integer', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addIndex(['artist_id','position'])
            ->addIndex(['artist_id','item_id'])
            ->addIndex(['item_id','artist_id'])
            ->addIndex(['artist_id','typeitem_id','position'],['unique' => true])
            ->save();

        $table = $this->table('items_bands');
        $table->addColumn('band_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('band_id', 'bands', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('item_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('item_id', 'items', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('typeitem_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addForeignKey('typeitem_id', 'typeitems', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addColumn('position', 'integer', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addIndex(['band_id','position'])
            ->addIndex(['band_id','item_id'])
            ->addIndex(['item_id','band_id'])
            ->addIndex(['band_id','typeitem_id','position'],['unique' => true])
            ->save();
    }

    /**
     * Método para escribir los cambios a realizar en caso de generar un rollback
     */
    public function down () {
        //Ojo cuando hacemos un rollback, eliminar primero las tablas con claves
        //ajenas y después las tablas maestras.

        $this->dropTable('configurations');
        $this->dropTable('items_artists');
        $this->dropTable('items_bands');
    }
}
