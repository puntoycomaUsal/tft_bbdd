<?php


use Phinx\Migration\AbstractMigration;

class V6Triggers extends AbstractMigration
{
    /** 
     * Ignoramos el uso del método change() ya que si usamos la función
     * up() escribimos las migraciones en ella y en down() escribiremos
     * las acciones si necesitamos realizar un rollback
    */

    /**
     * Método para escribir las migraciones
     */
    public function up ()
    {   
        // Insertar procedimientos & triggers:
        // Ojo! según he leído no se puede usar DELIMITERs pero tampoco es necesario
        $script = "
			-- PROCEDIMIENTOS

			-- Procedimiento checkTypeimteItem comprobará que para el id del item dado
			-- tiene el tipo de item especificado como parámetro.
			CREATE PROCEDURE checkTypeimteItem (IN item_id INT,IN typeitem_id INT)
			BEGIN
				-- Declaración de variables
				DECLARE total_items INT;
				DECLARE msg VARCHAR(250);

				-- Buscamos ahora en la tabla de items
				SET total_items = (SELECT COUNT(*)
									FROM items
									WHERE items.id = item_id AND items.typeitem_id = typeitem_id);
									
				-- Tiene que existir un solo registro
				IF total_items <> 1 THEN
					SET msg	= CONCAT('TriggerError: El item ',item_id,' no es del tipo de item ',typeitem_id,' especificado');
					SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
				END IF;
			END;


			-- TRIGGERS

			CREATE TRIGGER items_albumsBeforeInsert BEFORE INSERT ON items_albums
				FOR EACH ROW 
					CALL checkTypeimteItem(NEW.item_id, NEW.typeitem_id);
			CREATE TRIGGER items_artistsBeforeInsert BEFORE INSERT ON items_artists
				FOR EACH ROW 
					CALL checkTypeimteItem(NEW.item_id, NEW.typeitem_id);
			CREATE TRIGGER items_bandsBeforeInsert BEFORE INSERT ON items_bands
				FOR EACH ROW 
					CALL checkTypeimteItem(NEW.item_id, NEW.typeitem_id);
					        ";

        $this->query($script);
    }

    /**
     * Método para escribir los cambios a realizar en caso de generar un rollback
     */
    public function down () {
        //Ojo cuando hacemos un rollback, eliminar primero las tablas con claves
        //ajenas y después las tablas maestras.

		//Procedimientos
        $this->execute('DROP PROCEDURE checkTypeimteItem');
        
        //Disparadores
        $this->execute('DROP TRIGGER items_albumsBeforeInsert');
        $this->execute('DROP TRIGGER items_artistsBeforeInsert');
        $this->execute('DROP TRIGGER items_bandsBeforeInsert');
        

    }
}
