<?php


use Phinx\Migration\AbstractMigration;

class V6Datas extends AbstractMigration
{
    /** 
     * Ignoramos el uso del método change() ya que si usamos la función
     * up() escribimos las migraciones en ella y en down() escribiremos
     * las acciones si necesitamos realizar un rollback
    */

    /**
     * Método para escribir las migraciones
     */
    public function up () {
        
        //Generar los endpoints
        $endpoints            	= [
            ['Albums'            =>'/album'],
            ['Albumcollections'  =>'/albumcollection'],
            ['Artists'           =>'/artist'],
            ['Artistcollections' =>'/artistcollection'],
            ['Configurations'    =>'/configuration'],
            ['Bands'             =>'/band'],
            ['BandsArtists'      =>'/band/artist'],
            ['Bandcollections'   =>'/bandcollection'],
            ['Items'             =>'/item'],
            ['ItemsArtists'      =>'/item/artist'],
            ['ItemsAlbums'       =>'/item/album'],
            ['ItemsBands'        =>'/item/band'],
            ['Newcollections'    =>'/newcollection'],
            ['Tags'              =>'/tag'],
            ['TagsAlbums'        =>'/tag/album'],
            ['TagsBands'         =>'/tag/band'],
            ['Typebands'         =>'/typeband'],
            ['Typeitems'         =>'/typeitem'],
            ['Roles'             =>'/role'],
        ];
        //Generar redes sociales
        $rrss					= [
			['facebook'			=> 'https://wwww.facebook.es/puntoycoma'],
			['facebook'			=> 'https://wwww.twitter.es/puntoycoma'],        
        ];

        //Insertar datos typeitems
        $query = 'DELETE FROM configurations';
        $this->execute($query);
        
        $query = 'ALTER TABLE configurations AUTO_INCREMENT = 0';
        $this->execute($query);
        $rows = [
            [
                'pathResources'     => '/var/www/html/resources',
                'urlResources'     	=> 'http://localhost/resources',
                'urlApi'        	=> 'http://localhost:8500',
                'endpoints'     	=> json_encode($endpoints),
                'rrss'				=> json_encode($rrss),
            ]
        ];
        $this->table('configurations')->insert($rows)->save();
                
    }

    /**
     * Método para escribir los cambios a realizar en caso de generar un rollback
     */
    public function down () {
        //Ojo cuando hacemos un rollback, eliminar primero las tablas con claves
        //ajenas y después las tablas maestras.

        $this->execute('DELETE FROM configurations');

    }
}
