<?php


use Phinx\Migration\AbstractMigration;

class V4Datas extends AbstractMigration
{
    /** 
     * Ignoramos el uso del método change() ya que si usamos la función
     * up() escribimos las migraciones en ella y en down() escribiremos
     * las acciones si necesitamos realizar un rollback
    */

    /**
     * Método para escribir las migraciones
     */
    public function up () {

        //tags
        $rows = [
            [
                'name'         =>  'Estilo 1',
            ],
            [
                'name'         =>  'Estilo 2',
            ],
            [
                'name'         =>  'Estilo 3',
            ],
        ];
        $this->table('tags')->insert($rows)->save();
        
        //tags_albums
        $rows = [
            [
                'tag_id'         =>  1,
                'album_id'    =>  1,
            ],
            [
                'tag_id'         =>  2,
                'album_id'    =>  2,
            ],
            [
                'tag_id'         =>  3,
                'album_id'    =>  2,
            ],
        ];
        $this->table('tags_albums')->insert($rows)->save();
        
        //tags_bands
        $rows = [
            [
                'tag_id'        =>  1,
                'band_id'     =>  4,
            ],
            [
                'tag_id'        =>  2,
                'band_id'     =>  2,
            ],
            [
                'tag_id'        =>  3,
                'band_id'     =>  3,
            ],
            [
                'tag_id'        =>  3,
                'band_id'     =>  1,
            ],
        ];
        $this->table('tags_bands')->insert($rows)->save();
        
    }

    /**
     * Método para escribir los cambios a realizar en caso de generar un rollback
     */
    public function down () {
        //Ojo cuando hacemos un rollback, eliminar primero las tablas con claves
        //ajenas y después las tablas maestras.

        $this->execute('DELETE FROM tags');
        $this->execute('DELETE FROM tags_albums');
        $this->execute('DELETE FROM tags_bands');
    }
}
