<?php


use Phinx\Migration\AbstractMigration;

class V2Datas extends AbstractMigration
{
    /** 
     * Ignoramos el uso del método change() ya que si usamos la función
     * up() escribimos las migraciones en ella y en down() escribiremos
     * las acciones si necesitamos realizar un rollback
    */

    /**
     * Método para escribir las migraciones
     */
    public function up () {

        //Generar los endpoints
        $endpoints            = [
            ['Albums'            =>'/album'],
            ['Artists'           =>'/artist'],
            ['Configurations'    =>'/configuration'],
            ['Bands'             =>'/band'],
            ['BandsArtists'       =>'/band/artist'],
            ['Items'             =>'/item'],
            ['ItemsArtists'             =>'/item/artist'],
            ['ItemsAlbums'        =>'/item/album'],
            ['ItemsBands'         =>'/item/band'],
            ['Typebands'         =>'/typeband'],
            ['Typeitems'         =>'/typeitem'],
            ['Roles'             =>'/role'],
        ];


        //Insertar datos typeitems
        $query = 'DELETE FROM configurations';
        $this->execute($query);
        
        $query = 'ALTER TABLE configurations AUTO_INCREMENT = 0';
        $this->execute($query);
        $rows = [
            [
                'pathResources'     =>  '/var/www/html/resources',
                'urlResources'     =>  'http://localhost/resources',
                'urlApi'        => 'http://localhost:8500',
                'endpoints'     => json_encode($endpoints),
            ]
        ];
        $this->table('configurations')->insert($rows)->save();
    }

    /**
     * Método para escribir los cambios a realizar en caso de generar un rollback
     */
    public function down () {
        //Ojo cuando hacemos un rollback, eliminar primero las tablas con claves
        //ajenas y después las tablas maestras.

        $this->execute('DELETE FROM configurations');
    }
}
