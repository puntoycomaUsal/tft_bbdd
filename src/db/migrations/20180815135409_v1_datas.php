<?php


use Phinx\Migration\AbstractMigration;

class V1Datas extends AbstractMigration
{
    /** 
     * Ignoramos el uso del método change() ya que si usamos la función
     * up() escribimos las migraciones en ella y en down() escribiremos
     * las acciones si necesitamos realizar un rollback
    */

    /**
     * Método para escribir las migraciones
     */
    public function up () {
        //Insertar datos typeitems
        $query = 'DELETE FROM typeitems';
        $this->execute($query);
        $query = 'ALTER TABLE typeitems AUTO_INCREMENT = 0';
        $this->execute($query);
        $rows = [
            [
              'name'  => 'texto'
            ],
            [
              'name'  => 'audio'
            ],
            [
              'name'  => 'imagen'
            ],
            [
              'name'  => 'video'
            ]
        ];
        $this->table('typeitems')->insert($rows)->save();

        //Insertar datos typebands
        $query = 'DELETE FROM typebands';
        $this->execute($query);
        $query = 'ALTER TABLE typebands AUTO_INCREMENT = 0';
        $this->execute($query);
        $rows = [
            [
              'name'  => 'solo'
            ],
            [
              'name'  => 'grupo'
            ]
        ];
        $this->table('typebands')->insert($rows)->save();
    }

    /**
     * Método para escribir los cambios a realizar en caso de generar un rollback
     */
    public function down () {
        //Ojo cuando hacemos un rollback, eliminar primero las tablas con claves
        //ajenas y después las tablas maestras.

        $this->execute('DELETE FROM typeitems');
        $this->execute('DELETE FROM typebands');

    }
}
