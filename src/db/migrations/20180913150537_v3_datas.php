<?php


use Phinx\Migration\AbstractMigration;

class V3Datas extends AbstractMigration
{

    public function up () {

        $rows = [
            [
                'id'   => 1,
                'name' => 'default'
            ],
        ];
        $this->table('roles')->insert($rows)->save();

        $rows = [
            [
                'id'             => 1,
                'first_name'     => 'Bob',
                'last_name'      => 'Esponja',
                'birth_date'     => '2017-09-03',
                'birth_place'    => 'Piña',
                'biography'      => 'Es el protagonista de la serie. Él es una esponja marina optimista, graciosa, y enérgica que físicamente se parece a una esponja artificial, como una de cocina y vive en una casa con forma de piña. A menudo se muestra en la serie en su trabajo en el Crustáceo Crujiente como cocinero. Las aficiones de Bob Esponja en la serie incluyen la práctica de karate con Arenita, la captura de medusas y soplar burbujas. Asiste a la Escuela de Navegación de la Sra. Puff, pero nunca ha pasado la clase de manejo y su falta de su licencia de conducir sirve como un gag en toda la serie.',
                'web'            => 'http://bobesponjalaesponja.blogspot.com/'
            ],
            [
                'id'             => 2,
                'first_name'     => 'Patricio',
                'last_name'      => 'Rosa',
                'birth_date'     => '2017-09-03',
                'birth_place'    => 'Piedra',
                'biography'      => 'Es una estrella de mar rosa, perezoso, con sobrepeso, y es el mejor amigo de Bob Esponja. Vive debajo de una roca y su rasgo más destacado es su escasa capacidad de concentración, y su poca inteligencia que a veces llega a ser muy estúpido. Representado como una influencia negativa en Bob Esponja, que fomenta las actividades peligrosas y estúpidas, en muchas ocasiones sus actividades le ocasionan giro a alguna trama. Aunque normalmente es desempleado en todo el transcurso de la serie, Patricio tiene varios trabajos a corto plazo como la trama de cada episodio requiere. Patricio se ha referido tanto a estrella de mar y estrellas de mar en toda la serie, por lo que es inconcluso de su especie oficial. Además es muy bobo pero a la vez gracioso.',
                'web'            => 'http://bobesponjalaesponja.blogspot.com/'
            ],
            [
                'id'             => 3,
                'first_name'     => 'Calamardo',
                'last_name'      => 'Azul',
                'birth_date'     => '2016-09-03',
                'birth_place'    => 'Mar',
                'biography'      => 'Es un decadente vecino cefalópodo de Bob Esponja y Patricio. Calamardo se ha referido como un calamar por el creador de la serie. Vive en una casa con forma de un Moay de Isla de Pascua. Trabaja como cajero en el CrustáceoCrujiente , un trabajo que no le gusta. Bob Esponja y Patricio lo consideran su amigo, aunque él los odia; pero en el fondo los quiere. Su sueño es ser un excelente músico, clarinetista, o pintor.',
                'web'            => 'http://bobesponjalaesponja.blogspot.com/'
            ],
            [
                'id'             => 4,
                'first_name'     => 'Sr',
                'last_name'      => 'Cangrejo',
                'birth_date'     => '2015-09-03',
                'birth_place'    => 'Tierra',
                'biography'      => 'Es un cangrejo muy codicioso y tacaño con el dinero que gana (a veces incluso hasta el punto de la locura) y detesta gastarlo. Él es el dueño del Crustáceo Cascarudo, así como empresario y jefe de Bob Esponja y Calamardo. El egoísmo y codicia de Don Cangrejo es tan extremo que antropomorfiza dinero, y actúa con poco respeto hacia otras personas cuando se enfrentan a perder dinero e incluso utiliza a Bob Esponja en sus planes de ganar más dinero, pero termina siendo traicionado y humillado por el que termina perdiendo todo el dinero que ganó. Él tiene una hijastra adolescente llamada Perla (Lori Alan), a pesar de que es una ballena y no ha habido ninguna pista si Don Cangrejo tiene una esposa.',
                'web'            => 'http://bobesponjalaesponja.blogspot.com/'
            ]
        ];
        $this->table('artists')->insert($rows)->save();
    
        $rows = [
            [
                'id'               => 1,
                'typeband_id'      => 1,
                'name'             => 'La música en el Campo',
                'foundation_date'  => '1717-09-03',
                'web'              => 'https://www.melomanodigital.com/'
            ],
            [
                'id'               => 2,
                'typeband_id'      => 2,
                'name'             => 'La Escuela veneciana',
                'foundation_date'  => '1817-09-03',
                'web'              => 'https://www.melomanodigital.com/'
            ],
            [
                'id'               => 3,
                'typeband_id'      => 2,
                'name'             => 'El Desarrollo de los Instrumentos Barrocos',
                'foundation_date'  => '1617-09-03',
                'web'              => 'https://www.melomanodigital.com/'
            ],
            [
                'id'               => 4,
                'typeband_id'      => 1,
                'name'             => 'La corte de Luis XIV',
                'foundation_date'  => '1917-09-03',
                'web'              => 'https://www.melomanodigital.com/'
            ]
        ];
        $this->table('bands')->insert($rows)->save();

        $rows = [
            [
                'id'               => 1,
                'name'             => 'La discriminación de las piñas',
                'publication_date'  => '1717-09-03',
                'web'              => 'https://www.melomanodigital.com/'
            ],
            [
                'id'               => 2,
                'name'             => 'Muerte por aplastamiento',
                'publication_date'  => '1817-09-03',
                'web'              => 'https://www.melomanodigital.com/'
            ],
            [
                'id'               => 3,
                'name'             => 'Hamburguesas',
                'publication_date'  => '1617-09-03',
                'web'              => 'https://www.melomanodigital.com/'
            ]
        ];
        $this->table('albums')->insert($rows)->save();

        $rows = [
            [
                'id'           => 1,
                'typeitem_id'  => 2,
                'name'         => 'finaton',
                'band_id'      => 1,
                'url'          => '/audio/dp_01.mp3'
            ],
            [
                'id'           => 2,
                'typeitem_id'  => 2,
                'name'         => 'forke',
                'band_id'      => 1,
                'url'          => '/audio/dp_02.mp3'
            ],
            [
                'id'           => 3,
                'typeitem_id'  => 2,
                'name'         => 'pinaiki',
                'band_id'      => 2,
                'url'          => '/audio/ma_01.mp3'
            ],
            [
                'id'           => 4,
                'typeitem_id'  => 2,
                'name'         => 'shojine',
                'band_id'      => 2,
                'url'          => '/audio/ma_02.mp3'
            ],
            [
                'id'           => 5,
                'typeitem_id'  => 2,
                'name'         => 'cittiong',
                'band_id'      => 2,
                'url'          => '/audio/ma_03.mp3'
            ],
            [
                'id'           => 6,
                'typeitem_id'  => 2,
                'name'         => 'metim',
                'band_id'      => 2,
                'url'          => '/audio/ma_04.mp3'
            ],
            [
                'id'           => 7,
                'typeitem_id'  => 2,
                'name'         => 'siell',
                'band_id'      => 3,
                'url'          => '/audio/h_01.mp3'
            ],
            [
                'id'           => 8,
                'typeitem_id'  => 2,
                'name'         => 'moung',
                'band_id'      => 3,
                'url'          => '/audio/h_02.mp3'
            ],
            [
                'id'           => 9,
                'typeitem_id'  => 2,
                'name'         => 'indrotare',
                'band_id'      => 3,
                'url'          => '/audio/h_03.mp3'
            ]
        ];
        $this->table('items')->insert($rows)->save();

        $rows = [
            [
                'id'            => 1,
                'album_id'      => 1,
                'item_id'       => 1,
                'typeitem_id'   => 2,
                'position'      => 1,
            ],
            [
                'id'            => 2,
                'album_id'      => 1,
                'item_id'       => 2,
                'typeitem_id'   => 2,
                'position'      => 2,
            ],
            [
                'id'            => 3,
                'album_id'      => 1,
                'item_id'       => 3,
                'typeitem_id'   => 2,
                'position'      => 3,
            ],
            [
                'id'            => 4,
                'album_id'      => 2,
                'item_id'       => 4,
                'typeitem_id'   => 2,
                'position'      => 1,
            ],
            [
                'id'            => 5,
                'album_id'      => 2,
                'item_id'       => 5,
                'typeitem_id'   => 2,
                'position'      => 2,
            ],
            [
                'id'            => 6,
                'album_id'      => 2,
                'item_id'       => 6,
                'typeitem_id'   => 2,
                'position'      => 3,
            ],
            [
                'id'            => 7,
                'album_id'      => 3,
                'item_id'       => 7,
                'typeitem_id'   => 2,
                'position'      => 1,
            ],
            [
                'id'            => 8,
                'album_id'      => 3,
                'item_id'       => 8,
                'typeitem_id'   => 2,
                'position'      => 2,
            ],
            [
                'id'            => 9,
                'album_id'      => 3,
                'item_id'       => 9,
                'typeitem_id'   => 2,
                'position'      => 3,
            ]
        ];
        $this->table('items_albums')->insert($rows)->save();

        $rows = [
            [
                'id'         => 1,
                'artist_id'  => 1,
                'band_id'    => 1,
                'role_id'    => 1,
                'start_date' => '2017-09-03',
                'end_date'   => '2018-09-03'
            ],
            [
                'id'         => 2,
                'artist_id'  => 2,
                'band_id'    => 2,
                'role_id'    => 1,
                'start_date' => '2016-09-03',
                'end_date'   => '2017-09-03'
            ],
            [
                'id'         => 3,
                'artist_id'  => 3,
                'band_id'    => 3,
                'role_id'    => 1,
                'start_date' => '2016-09-03',
                'end_date'   => '2017-09-03'
            ]
        ];
        $this->table('bands_artists')->insert($rows)->save();

    }


    public function down () {
        $this->execute('DELETE FROM bands_artists');
        $this->execute('DELETE FROM items_albums');
        $this->execute('DELETE FROM items');
        $this->execute('DELETE FROM artists');
        $this->execute('DELETE FROM bands');
        $this->execute('DELETE FROM albums');
        $this->execute('DELETE FROM roles');
    }
}
