<?php


use Phinx\Migration\AbstractMigration;

class V5Datas extends AbstractMigration
{
    /** 
     * Ignoramos el uso del método change() ya que si usamos la función
     * up() escribimos las migraciones en ella y en down() escribiremos
     * las acciones si necesitamos realizar un rollback
    */

    /**
     * Método para escribir las migraciones
     */
    public function up () {

        $rows = [
            [
                'id'               => 4,
                'name'             => 'Vamos que nos vamos!',
                'publication_date'  => '1717-09-03',
                'web'              => 'https://www.nos.com/'
            ],
            [
                'id'               => 5,
                'name'             => 'Muerte y destrución',
                'publication_date'  => '1817-09-03',
                'web'              => 'https://www.muerte.com/'
            ],
            [
                'id'               => 6,
                'name'             => 'El que no corre vuela.',
                'publication_date'  => '1617-09-03',
                'web'              => 'https://www.vuela.com/'
            ],
            [
                'id'               => 7,
                'name'             => 'Por que si!',
                'publication_date'  => '1717-09-03',
                'web'              => 'https://www.nos.com/'
            ],
            [
                'id'               => 8,
                'name'             => 'Perfecto',
                'publication_date'  => '1817-09-03',
                'web'              => 'https://www.muerte.com/'
            ],
            [
                'id'               => 9,
                'name'             => 'Amen.',
                'publication_date'  => '1617-09-03',
                'web'              => 'https://www.vuela.com/'
            ]
        ];
        $this->table('albums')->insert($rows)->save();

        $rows = [
            [
                'id'           => 14,
                'typeitem_id'  => 3,
                'name'         => 'metim',
                'band_id'      => 1,
                'url'          => '/image/a_01.jpg'
            ],
            [
                'id'           => 15,
                'typeitem_id'  => 3,
                'name'         => 'siell',
                'band_id'      => 2,
                'url'          => '/image/a_02.jpg'
            ],
            [
                'id'           => 16,
                'typeitem_id'  => 3,
                'name'         => 'moung',
                'band_id'      => 3,
                'url'          => '/image/a_03.jpg'
            ]
        ];
        $this->table('items')->insert($rows)->save();

        $rows = [
            [
                'id'            => 14,
                'album_id'      => 1,
                'item_id'       => 14,
                'typeitem_id'  	=> 3,
                'position'      => 14,
            ],
            [
                'id'            => 23,
                'album_id'      => 2,
                'item_id'       => 15,
                'typeitem_id'  	=> 3,
                'position'      => 15,
            ],
            [
                'id'            => 33,
                'album_id'      => 3,
                'item_id'       => 16,
                'typeitem_id'  	=> 3,
                'position'      => 12,
            ]
        ];
        $this->table('items_albums')->insert($rows)->save();

    }

    /**
     * Método para escribir los cambios a realizar en caso de generar un rollback
     */
    public function down () {
    }
}
