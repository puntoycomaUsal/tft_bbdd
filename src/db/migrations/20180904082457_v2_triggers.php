<?php


use Phinx\Migration\AbstractMigration;

class V2Triggers extends AbstractMigration
{
    /** 
     * Ignoramos el uso del método change() ya que si usamos la función
     * up() escribimos las migraciones en ella y en down() escribiremos
     * las acciones si necesitamos realizar un rollback
    */

    /**
     * Método para escribir las migraciones
     */
    public function up ()
    {   
        // Insertar procedimientos & triggers:
        // Ojo! según he leído no se puede usar DELIMITERs pero tampoco es necesario
        $script = "
        -- FUNCIONES & PROCEDIMIENTOS

        -- Procedimiento que devolverá un error si el tipo es audio = id = 2
        DROP PROCEDURE IF EXISTS checkTypeItemAudio;
        CREATE PROCEDURE checkTypeItemAudio(IN item_id INT(11))
        BEGIN
            DECLARE msg VARCHAR(250);
            DECLARE totalRows INT(6);
            SET totalRows   = (SELECT COUNT(*)
                                FROM items
                                WHERE items.id = item_id and items.typeitem_id = 2);
            IF totalRows > 0 THEN
                SET msg = 'TriggerError: El tipo de item no puede ser audio';
                SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
            END IF;
        END;

        -- Procedimiento que devolverá un error si la tabla de configuración tiene
        -- ya un registro
        DROP PROCEDURE IF EXISTS checkConfigurationsTable;
        CREATE PROCEDURE checkConfigurationsTable()
        BEGIN
            DECLARE msg VARCHAR(250);
            DECLARE totalRows INT(6);
            SET totalRows   = (SELECT COUNT(*)
                                FROM configurations);
            IF totalRows > 0 THEN
                SET msg = 'TriggerError: La tabla de configuración ya contiene un registro';
                SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
            END IF;
        END;

        -- DISPARADORES

        -- CONFIGURATIONS
        DROP TRIGGER IF EXISTS ConfigurationsBeforeInsert;
        CREATE TRIGGER ConfigurationsBeforeInsert BEFORE INSERT ON configurations
            FOR EACH ROW CALL checkConfigurationsTable();
        
        -- ITEMS_ARTISTS
        DROP TRIGGER IF EXISTS Items_ArtistsBeforeInsert;
        CREATE TRIGGER Items_ArtistsBeforeInsert BEFORE INSERT ON items_artists
            FOR EACH ROW CALL checkTypeItemAudio(NEW.item_id);
        
        DROP TRIGGER IF EXISTS Items_ArtistsBeforeUpdate;
        CREATE TRIGGER Items_ArtistsBeforeUpdate BEFORE UPDATE ON items_artists
            FOR EACH ROW CALL checkTypeItemAudio(NEW.item_id);
        
        -- ITEMS_BANDS
        DROP TRIGGER IF EXISTS Items_BandsBeforeInsert;
        CREATE TRIGGER Items_BandsBeforeInsert BEFORE INSERT ON items_bands
            FOR EACH ROW CALL checkTypeItemAudio(NEW.item_id);
        
        DROP TRIGGER IF EXISTS Items_BandsBeforeUpdate;
        CREATE TRIGGER Items_BandsBeforeUpdate BEFORE UPDATE ON items_bands
            FOR EACH ROW CALL checkTypeItemAudio(NEW.item_id);

        ";

        $this->query($script);
    }

    /**
     * Método para escribir los cambios a realizar en caso de generar un rollback
     */
    public function down () {
        //Ojo cuando hacemos un rollback, eliminar primero las tablas con claves
        //ajenas y después las tablas maestras.

        $this->execute('DROP PROCEDURE checkTypeItemAudio');
        $this->execute('DROP PROCEDURE checkConfigurationsTable');
        $this->execute('DROP TRIGGER ConfigurationsBeforeInsert');
        $this->execute('DROP TRIGGER Items_ArtistsBeforeInsert');
        $this->execute('DROP TRIGGER Items_ArtistsBeforeUpdate');
        $this->execute('DROP TRIGGER Items_BandsBeforeInsert');
        $this->execute('DROP TRIGGER Items_BandsBeforeUpdate');

    }
}
